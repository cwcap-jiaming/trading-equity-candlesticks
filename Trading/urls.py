from django.conf.urls import patterns, include, url
from django.contrib import admin
import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = patterns('',
    # Examples:
    # url(r'^blog/', include('blog.urls')), 
    url(r'^$', views.equityhomepage, name='equityhomepage'),
    url(r'^equity/(?P<account_number>\d+)/$', views.candle_chart, name='candle_chart'),
    url(r'^equity/compare/$', views.comparison_chart, name='comparison_chart'),
    url(r'^accounts/login', views.login, name='login'),
    url(r'^accounts/setequity', views.setequity, name='setequity'),
    url(r'^admin/', include(admin.site.urls)),
)

urlpatterns += staticfiles_urlpatterns()
